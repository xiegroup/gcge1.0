#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>


#include "ops.h"
#include "app_ccs.h"
#include "ops_eig_sol_gcg.h"
#include "app_lapack.h"


int TestVec              (void *mat, struct OPS_ *ops);
int TestMultiVec         (void *mat, struct OPS_ *ops);
int TestOrth             (void *mat, struct OPS_ *ops);
int TestLinearSolver     (void *mat, struct OPS_ *ops);
int TestMultiLinearSolver(void *mat, struct OPS_ *ops);
int TestEigenSolverGCG   (void *A, void *B, int flag, int argc, char *argv[], struct OPS_ *ops);
int TestModelEigenSolverGCG(void* A, void* B, int flag, int q, struct OPS_* ops, double tol, double* eval, double** evec);
int TestMultiGrid        (void *A, void *B, struct OPS_ *ops);

static int CreateMatrixModelCCS(double *A_data, int*A_i_row, int *A_j_col, double *B_data, int*B_i_row, int *B_j_col, int n, CCSMAT *ccs_matA, CCSMAT *ccs_matB);
static int DestroyMatrixCCS(CCSMAT *ccs_matA, CCSMAT *ccs_matB);

int TestAppModelCCS(double *A_data, int*A_i_row, int *A_j_col, double *B_data, int*B_i_row, int *B_j_col, int n, int q,  double tol, double *eval, double *evec) 
{
#if OPS_USE_MPI
   MPI_Init(&argc, &argv);
#endif

   OPS *ccs_ops = NULL;
   OPS_Create (&ccs_ops);
   OPS_CCS_Set (ccs_ops);
   OPS_Setup (ccs_ops);

   void *matA, *matB; OPS *ops;

   CCSMAT ccs_matA, ccs_matB;
   CreateMatrixModelCCS(A_data, A_i_row, A_j_col, B_data, B_i_row, B_j_col,n,&ccs_matA, &ccs_matB);

   LAPACKVEC* LAPACK_evec = malloc(sizeof(LAPACKVEC));
   LAPACK_evec->nrows = n;
   LAPACK_evec->ncols = 2*q;
   LAPACK_evec->ldd = n;
   LAPACK_evec->data = evec;
      
   ops = ccs_ops; matA = (void*)(&ccs_matA); matB = (void*)(&ccs_matB);
   /* flag == 0 
    * flag == 1 
    * flag == 2 */
   int flag = 0;


   TestModelEigenSolverGCG(matA,matB, flag, q, ops,tol, eval, LAPACK_evec);
   OPS_Destroy(&ccs_ops);
   free(LAPACK_evec);
#if OPS_USE_MPI
   MPI_Finalize();
#endif
   return 1;
}

//Input: double[] K_a, int[] K_ai, int[] K_aj, double[] M_a, int[] M_ai, int[] M_aj, int
//Output: ccs_matA, ccs_matB
static int CreateMatrixModelCCS(double *A_data, int*A_i_row, int *A_j_col, double *B_data, int*B_i_row, int *B_j_col, int n, CCSMAT *ccs_matA, CCSMAT *ccs_matB) 
{
   ccs_matA->nrows = n; ccs_matA->ncols = n;
   ccs_matA->j_col = A_j_col; 
   ccs_matA->i_row = A_i_row; 
   ccs_matA->data  = A_data;   

   ccs_matB->nrows = n; ccs_matB->ncols = n;
   ccs_matB->j_col = B_j_col; 
   ccs_matB->i_row = B_i_row; 
   ccs_matB->data  = B_data; 
   return 0;
}


static int DestroyMatrixCCS(CCSMAT *ccs_matA, CCSMAT *ccs_matB)
{
   free(ccs_matA->i_row); ccs_matA->i_row = NULL;
   free(ccs_matB->i_row); ccs_matB->i_row = NULL;
   free(ccs_matA->j_col); ccs_matA->j_col = NULL;
   free(ccs_matB->j_col); ccs_matB->j_col = NULL;
   free(ccs_matA->data) ; ccs_matA->data  = NULL;
   free(ccs_matB->data) ; ccs_matB->data  = NULL;
   return 0;
}
