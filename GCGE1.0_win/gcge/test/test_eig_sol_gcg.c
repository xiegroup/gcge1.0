

#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <memory.h>
#include <time.h>
#include "ops.h"
#include "ops_eig_sol_gcg.h"


#define DEBUG   0

int TestEigenSolverGCG(void *A, void *B, int flag, int argc, char *argv[], struct OPS_ *ops) 
{
	int nevConv  = 30, multiMax = 1; double gapMin = 1e-5;
	int nevGiven = 0, block_size = nevConv/5, nevMax = 2*nevConv; 
	int nevInit = nevMax;

	nevMax = 2*nevConv;
	block_size = nevConv<30?(nevMax-nevConv):nevConv/5;
	nevInit = nevMax;
	nevInit = nevInit<nevMax?nevInit:nevMax;
	int max_iter_gcg = 500; double tol_gcg[2] = {1e-1,1e-8};
	double *eval; void **evec;
	eval = malloc(nevMax*sizeof(double));
	memset(eval,0,nevMax*sizeof(double));
	ops->MultiVecCreateByMat(&evec,nevMax,A,ops);
	ops->MultiVecSetRandomValue(evec,0,nevMax,ops);
	void **gcg_mv_ws[4]; double *dbl_ws; int *int_ws;

	ops->MultiVecCreateByMat(&gcg_mv_ws[0],nevMax+2*block_size,A,ops);				
	ops->MultiVecSetRandomValue(gcg_mv_ws[0],0,nevMax+2*block_size,ops);
	ops->MultiVecCreateByMat(&gcg_mv_ws[1],block_size,A,ops);				
	ops->MultiVecSetRandomValue(gcg_mv_ws[1],0,block_size,ops);
	ops->MultiVecCreateByMat(&gcg_mv_ws[2],block_size,A,ops);				
	ops->MultiVecSetRandomValue(gcg_mv_ws[2],0,block_size,ops);
	ops->MultiVecCreateByMat(&gcg_mv_ws[3],block_size,A,ops);				
	ops->MultiVecSetRandomValue(gcg_mv_ws[3],0,block_size,ops);
	int sizeV = nevInit + 2*block_size;
	int length_dbl_ws = 2*sizeV*sizeV+10*sizeV
		+(nevMax+2*block_size)+(nevMax)*block_size;
	ops->Printf ( "length_dbl_ws = %d\n", length_dbl_ws );
	int length_int_ws = 6*sizeV+2*(block_size+3);
	ops->Printf ( "length_int_ws = %d\n", length_int_ws );
	dbl_ws = malloc(length_dbl_ws*sizeof(double));
	memset(dbl_ws,0,length_dbl_ws*sizeof(double));
	int_ws = malloc(length_int_ws*sizeof(int));
	memset(int_ws,0,length_int_ws*sizeof(int));

	ops->Printf("mat A:\n");
	//ops->MatView(A,ops);
	if (B!=NULL) {
		ops->Printf("mat B:\n");
	//	ops->MatView(B,ops);
	}

	srand(0);
	double time_start, time_interval;
	time_start = ops->GetWtime();
		
	ops->Printf("===============================================\n");
	ops->Printf("GCG Eigen Solver\n");
	EigenSolverSetup_GCG(multiMax,gapMin,nevInit,nevMax,block_size,
		tol_gcg,max_iter_gcg,flag,gcg_mv_ws,dbl_ws,int_ws,ops);
	
	int    check_conv_max_num    = 50   ;
		
	char   initX_orth_method[8]  = "mgs"; 
	int    initX_orth_block_size = 80   ; 
	int    initX_orth_max_reorth = 2    ; double initX_orth_zero_tol    = 2*DBL_EPSILON;//1e-12
	
	char   compP_orth_method[8]  = "mgs"; 
	int    compP_orth_block_size = -1   ; 
	int    compP_orth_max_reorth = 2    ; double compP_orth_zero_tol    = 2*DBL_EPSILON;//1e-12
	
	char   compW_orth_method[8]  = "mgs";
	int    compW_orth_block_size = 80   ; 	
	int    compW_orth_max_reorth = 2    ;  double compW_orth_zero_tol   = 2*DBL_EPSILON;//1e-12
	int    compW_bpcg_max_iter   = 30   ;  double compW_bpcg_rate       = 1e-2; 
	double compW_bpcg_tol        = 1e-14;  char   compW_bpcg_tol_type[8] = "abs";
	
	int    compRR_min_num        = -1   ;  double compRR_min_gap        = gapMin;
	double compRR_tol            = 2*DBL_EPSILON;
	//double compRR_tol            = 0.0  ; 
			
	EigenSolverSetParameters_GCG(
			check_conv_max_num   ,
			initX_orth_method    , initX_orth_block_size, 
			initX_orth_max_reorth, initX_orth_zero_tol  ,
			compP_orth_method    , compP_orth_block_size, 
			compP_orth_max_reorth, compP_orth_zero_tol  ,
			compW_orth_method    , compW_orth_block_size, 
			compW_orth_max_reorth, compW_orth_zero_tol  ,
			compW_bpcg_max_iter  , compW_bpcg_rate      , 
			compW_bpcg_tol       , compW_bpcg_tol_type  , 0, // without shift
			compRR_min_num       , compRR_min_gap       ,
			compRR_tol           ,  
			ops);		

	EigenSolverSetParametersFromCommandLine_GCG(argc,argv,ops);
	ops->Printf("nevGiven = %d, nevConv = %d, nevMax = %d, block_size = %d, nevInit = %d\n",
			nevGiven,nevConv,nevMax,block_size,nevInit);
	ops->EigenSolver(A,B,eval,evec,nevGiven,&nevConv,ops);
	ops->Printf("numIter = %d, nevConv = %d\n",
			((GCGSolver*)ops->eigen_solver_workspace)->numIter, nevConv);
	ops->Printf("++++++++++++++++++++++++++++++++++++++++++++++\n");

	time_interval = ops->GetWtime() - time_start;
	ops->Printf("Time is %.3f\n", time_interval);

	ops->MultiVecDestroy(&gcg_mv_ws[0],nevMax+2*block_size,ops);
	ops->MultiVecDestroy(&gcg_mv_ws[1],block_size,ops);
	ops->MultiVecDestroy(&gcg_mv_ws[2],block_size,ops);
	ops->MultiVecDestroy(&gcg_mv_ws[3],block_size,ops);
	free(dbl_ws); free(int_ws);

#if 1
	ops->Printf("eigenvalues\n");
	int idx;
	for (idx = 0; idx < nevConv; ++idx) {
		ops->Printf("%d: %6.14e\n",idx+1,eval[idx]);
		//if (idx > 0)
		//	ops->Printf("%d: %6.14e\n",idx+1,(eval[idx]-eval[idx-1])/(eval[idx]+1));
	}
	ops->Printf("eigenvectors\n");
	//ops->MultiVecView(evec,0,nevConv,ops);
#endif

	ops->MultiVecDestroy(&(evec),nevMax,ops);
	free(eval);
	return 0;
}



int TestModelEigenSolverGCG(void *A, void *B, int flag, int q, struct OPS_ *ops, double tol, double *eval, void **evec) 
{
	/* nevConv, nevMax 
	 * block_size >= multiMax */ 
	int nevConv  = q, multiMax = 1; double gapMin = 1e-5;
	int nevGiven = 0, block_size = nevConv/3, nevMax = 2*nevConv;
	int nevInit = 3*block_size; 
	nevMax = nevConv + nevInit; 
	int max_iter_gcg = 500; double tol_gcg[2] = {1e+1,1e-5};
	tol_gcg[0] = 1e-6;
	tol_gcg[1] = 1e-7;
	ops->MultiVecSetRandomValue(evec,0,nevMax,ops);
	/* EigenSolverCreateWorkspace_GCG */
	void **gcg_mv_ws[4]; double *dbl_ws; int *int_ws;
	/* The dimension of the working space: nevMax+2*block_size, 
	 * block_size, block_size, block_size */
	ops->MultiVecCreateByMat(&gcg_mv_ws[0],nevMax+2*block_size,A,ops);				
	ops->MultiVecSetRandomValue(gcg_mv_ws[0],0,nevMax+2*block_size,ops);
	ops->MultiVecCreateByMat(&gcg_mv_ws[1],block_size,A,ops);				
	ops->MultiVecSetRandomValue(gcg_mv_ws[1],0,block_size,ops);
	ops->MultiVecCreateByMat(&gcg_mv_ws[2],block_size,A,ops);				
	ops->MultiVecSetRandomValue(gcg_mv_ws[2],0,block_size,ops);
	ops->MultiVecCreateByMat(&gcg_mv_ws[3],block_size,A,ops);				
	ops->MultiVecSetRandomValue(gcg_mv_ws[3],0,block_size,ops);
	int sizeV = nevInit + 2*block_size;
	int length_dbl_ws = 2*sizeV*sizeV+10*sizeV
		+(nevMax+2*block_size)+(nevMax)*block_size;
	//ops->Printf ( "length_dbl_ws = %d\n", length_dbl_ws );
	int length_int_ws = 6*sizeV+2*(block_size+3);
	//ops->Printf ( "length_int_ws = %d\n", length_int_ws );
	dbl_ws = malloc(length_dbl_ws*sizeof(double));
	memset(dbl_ws,0,length_dbl_ws*sizeof(double));
	int_ws = malloc(length_int_ws*sizeof(int));
	memset(int_ws,0,length_int_ws*sizeof(int));

	srand(0);
	double time_start, time_interval;
	time_start = ops->GetWtime();
		
	ops->Printf("===============================================\n");
	ops->Printf("GCG Eigen Solver\n");
	EigenSolverSetup_GCG(multiMax,gapMin,nevInit,nevMax,block_size,
		tol_gcg,max_iter_gcg,flag,gcg_mv_ws,dbl_ws,int_ws,ops);
	
	int    check_conv_max_num    = 50   ;
		
	char   initX_orth_method[8]  = "mgs"; 
	int    initX_orth_block_size = -1   ; 
	int    initX_orth_max_reorth = 2    ; double initX_orth_zero_tol    = 2*DBL_EPSILON;//1e-12
	
	char   compP_orth_method[8]  = "mgs"; 
	int    compP_orth_block_size = -1   ; 
	int    compP_orth_max_reorth = 2    ; double compP_orth_zero_tol    = 2*DBL_EPSILON;//1e-12
	
	char   compW_orth_method[8]  = "mgs";
	int    compW_orth_block_size = -1   ; 	
	int    compW_orth_max_reorth = 2    ;  double compW_orth_zero_tol   = 2*DBL_EPSILON;//1e-12
	int    compW_bpcg_max_iter   = 30  ;  double compW_bpcg_rate       = 1e-2; 
	double compW_bpcg_tol        = 1e-14;  char   compW_bpcg_tol_type[8] = "abs";
	
	int    compRR_min_num        = -1   ;  double compRR_min_gap        = gapMin;
	double compRR_tol            = 2*DBL_EPSILON;
	//double compRR_tol          = 0.0  ; 
	int    isShift = 1;  //表示是否做shift, 0表示不做shift，1表示做shift
	EigenSolverSetParameters_GCG(
			check_conv_max_num   ,
			initX_orth_method    , initX_orth_block_size, 
			initX_orth_max_reorth, initX_orth_zero_tol  ,
			compP_orth_method    , compP_orth_block_size, 
			compP_orth_max_reorth, compP_orth_zero_tol  ,
			compW_orth_method    , compW_orth_block_size, 
			compW_orth_max_reorth, compW_orth_zero_tol  ,
			compW_bpcg_max_iter  , compW_bpcg_rate      , 
			compW_bpcg_tol       , compW_bpcg_tol_type  , 
			isShift, // without shift
			compRR_min_num       , compRR_min_gap       ,
			compRR_tol           ,  
			ops);		

	//solve the eigenvalue problem
	//The current only includes GCG method
	
#if 0
	struct GCGSolver_* gcg_solver = (GCGSolver*)ops->eigen_solver_workspace;
	gcg_solver->tol[0] = 5e+3;
	gcg_solver->tol[1] = 1e-3;
	gcg_solver->numIterMax = 30;
	int idx;
	idx = nevConv;
	ops->EigenSolver(A, B, eval, evec, nevGiven, &idx, ops);
	gcg_solver->tol[0] = tol_gcg[0];
	gcg_solver->tol[1] = tol_gcg[1];
	gcg_solver->numIterMax = max_iter_gcg;
	
	//EigenSolverSetParametersFromCommandLine_GCG(argc, argv, ops);
	ops->EigenSolver(A, B, eval, evec, nevInit, &nevConv, ops);
#else
	ops->EigenSolver(A, B, eval, evec, nevGiven, &nevConv, ops);
#endif

	ops->Printf("numIter = %d, nevConv = %d\n",
			((GCGSolver*)ops->eigen_solver_workspace)->numIter, nevConv);
	ops->Printf("++++++++++++++++++++++++++++++++++++++++++++++\n");

	time_interval = ops->GetWtime() - time_start;
	ops->Printf("Time is %.3f\n", time_interval);
	ops->MultiVecDestroy(&gcg_mv_ws[0],nevMax+2*block_size,ops);
	ops->MultiVecDestroy(&gcg_mv_ws[1],block_size,ops);
	ops->MultiVecDestroy(&gcg_mv_ws[2],block_size,ops);
	ops->MultiVecDestroy(&gcg_mv_ws[3],block_size,ops);
	free(dbl_ws); free(int_ws);

	return 0;
}
