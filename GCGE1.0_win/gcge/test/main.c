#define _CRT_SECURE_NO_WARNINGS     //这个宏定义最好要放到.c文件的第一行
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ops.h"
#include "app_ccs.h"


int TestAppModelCCS(double* A_data, int* A_i_row, int* A_j_col, double* B_data, int* B_i_row, int* B_j_col, int n, int q, double tol, double* eval, double* evec);


int main(int argc, char* argv[])
{
    int n = 2000, col; //dimension of the matrix
    double h = 1.0 / (n + 1);
    CCSMAT ccs_matA, ccs_matB;
    int nev = 20; //number of required eigenvalue problem
    double tol_rel = 1e-8;
    //int n = 30000+7, col; double h = 1.0/(n+1);
    ccs_matA.nrows = n; ccs_matA.ncols = n;
    ccs_matA.j_col = malloc((n + 1) * sizeof(int));
    ccs_matA.i_row = malloc((3 * n - 2) * sizeof(int));
    ccs_matA.data = malloc((3 * n - 2) * sizeof(double));

    ccs_matA.j_col[0] = 0; ccs_matA.j_col[1] = 2;
    ccs_matA.i_row[0] = 0; ccs_matA.i_row[1] = 1;
    ccs_matA.data[0] = +2.0 / h;
    ccs_matA.data[1] = -1.0 / h;
    int idx = 2;
    for (col = 1; col < n - 1; ++col) {
        ccs_matA.j_col[col + 1] = ccs_matA.j_col[col] + 3;
        ccs_matA.i_row[idx + 0] = col - 1;
        ccs_matA.i_row[idx + 1] = col;
        ccs_matA.i_row[idx + 2] = col + 1;
        ccs_matA.data[idx + 0] = -1.0 / h;
        ccs_matA.data[idx + 1] = +2.0 / h;
        ccs_matA.data[idx + 2] = -1.0 / h;
        idx += 3;
    }
    ccs_matA.j_col[n] = ccs_matA.j_col[n - 1] + 2;
    ccs_matA.i_row[3 * n - 4] = n - 2;
    ccs_matA.i_row[3 * n - 3] = n - 1;
    ccs_matA.data[3 * n - 4] = -1.0 / h;
    ccs_matA.data[3 * n - 3] = +2.0 / h;

    ccs_matB.nrows = n; ccs_matB.ncols = n;
    ccs_matB.j_col = malloc((n + 1) * sizeof(int));
    ccs_matB.i_row = malloc(n * sizeof(int));
    ccs_matB.data = malloc(n * sizeof(double));
    for (col = 0; col < n; ++col) {
        ccs_matB.j_col[col] = col;
        ccs_matB.i_row[col] = col;
        ccs_matB.data[col] = 1.0 * h;
    }
    ccs_matB.j_col[n] = n;
#if 0
    TestAppCCS(argc, argv);
#else
    double* eval; double* evec;
    eval = (double*)calloc(2 * nev, sizeof(double));
    evec = (double*)calloc(2 * nev * n, sizeof(double));

    TestAppModelCCS(ccs_matA.data, ccs_matA.i_row, ccs_matA.j_col, ccs_matB.data, ccs_matB.i_row, ccs_matB.j_col,
        n, nev, tol_rel, eval, evec);

#endif
    return 0;
}
