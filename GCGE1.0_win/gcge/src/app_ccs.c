
#include	<stdio.h>
#include	<stdlib.h>
#include	<assert.h>
#include	<math.h>
#include	<memory.h> 
#include	"app_ccs.h"

#define MKL_INT int
#define MKL_INT double

static void MatView (CCSMAT *mat, struct OPS_ *ops)
{
	/* 第 i_row[i] 行, 第 j 列 元素非零, i data[i]
	 * j_col[j] <= i < j_col[j+1] */
	LAPACKVEC *multi_vec;
	ops->MultiVecCreateByMat((void ***)(&multi_vec), mat->ncols, mat, ops);
	int col, i; double *destin; 
	for (col = 0; col < mat->ncols; ++col) {
		for (i = mat->j_col[col]; i < mat->j_col[col+1]; ++i) {
			destin  = multi_vec->data+(multi_vec->ldd)*col+mat->i_row[i];
			*destin = mat->data[i];
		}
	}
	ops->lapack_ops->MatView((void *)multi_vec, ops->lapack_ops);
	ops->lapack_ops->MultiVecDestroy((void ***)(&multi_vec), mat->ncols, ops->lapack_ops);
	return;
}
/* multi-vec */
static void MultiVecCreateByMat (LAPACKVEC **des_vec, int num_vec, CCSMAT *src_mat, struct OPS_ *ops)
{
	(*des_vec)        = malloc(sizeof(LAPACKVEC));
	(*des_vec)->nrows = src_mat->ncols   ; 
	(*des_vec)->ncols = num_vec          ;
	(*des_vec)->ldd   = (*des_vec)->nrows;
	//printf("The size of evec: %d\n", ((*des_vec)->ldd) * ((*des_vec)->ncols));
	(*des_vec)->data  = malloc(((*des_vec)->ldd)*((*des_vec)->ncols)*sizeof(double));
	memset((*des_vec)->data,0,((*des_vec)->ldd)*((*des_vec)->ncols)*sizeof(double));
	return;
}
static void MatDotMultiVec (CCSMAT *mat, LAPACKVEC *x, 
		LAPACKVEC *y, int *start, int *end, struct OPS_ *ops)
{
	//printf("9999999999999999\n");
	assert(end[0]-start[0]==end[1]-start[1]);
	assert(y->nrows==y->ldd);
	assert(x->nrows==x->ldd);
	int num_vec = end[0]-start[0]; 
	//printf("num_vec = %d \n", num_vec);
	//int col; 
	if (mat!=NULL) {
#if OPS_USE_INTEL_MKL
	sparse_matrix_t csrA;
	struct matrix_descr descr;
	descr.type = SPARSE_MATRIX_TYPE_GENERAL;
	/*
	 * sparse_status_t mkl_sparse_d_create_csr (
	 *       sparse_matrix_t *A,  
	 *       const sparse_index_base_t indexing,  
	 *       const MKL_INT rows,  const MKL_INT cols,  
	 *       MKL_INT *rows_start,  MKL_INT *rows_end,  MKL_INT *col_indx,  double *values);
	 * sparse_status_t mkl_sparse_destroy (sparse_matrix_t A);
	 * sparse_status_t mkl_sparse_d_mm (
	 *       const sparse_operation_t operation,  
	 *       const double alpha,  
	 *       const sparse_matrix_t A,  const struct matrix_descr descr,  const sparse_layout_t layout,  
	 *       const double *B,  const MKL_INT columns,  const MKL_INT ldb,  
	 *       const double beta,  double *C,  const MKL_INT ldc);
	 */
	/* in process */
	//printf("create the mkl csr Matrix object\n");
	mkl_sparse_d_create_csr (
			&csrA,
			SPARSE_INDEX_BASE_ZERO,  
			mat->ncols,    //rows
			mat->nrows,    //cols
			mat->j_col,    //*row_start(每一行的开始位置)
			mat->j_col+ 1, //rows_end  (每一行的重点位置)
			mat->i_row,    //*col_index
			mat->data);    //*values
	int nnz = mat->j_col[mat->ncols]; 
	//printf("nnz = %d, data[end]=%f\n",nnz, mat->data[nnz-1]);
#if OPS_USE_OMP
	#pragma omp parallel num_threads(OMP_NUM_THREADS)
	{
		int id, length, offset;
		id     = omp_get_thread_num();
		length = num_vec/OMP_NUM_THREADS;
		offset = length*id;
		if (id < num_vec%OMP_NUM_THREADS) {
			++length; offset += id;
		}
		else {
			offset += num_vec%OMP_NUM_THREADS;
		} 
		/* 假设 mat 是对称矩阵, 否则 SPARSE_OPERATION_NON_TRANSPOSE 改为 SPARSE_OPERATION_TRANSPOSE */
		mkl_sparse_d_mm (
				SPARSE_OPERATION_NON_TRANSPOSE,
				1.0,
				csrA, descr, SPARSE_LAYOUT_COLUMN_MAJOR,  
				     x->data+(start[0]+offset)*x->ldd, length, x->ldd,  
				0.0, y->data+(start[1]+offset)*y->ldd, y->ldd);
	}
#else
	/* 假设 mat 是对称矩阵, 否则 SPARSE_OPERATION_NON_TRANSPOSE 改为 SPARSE_OPERATION_TRANSPOSE */
	//printf("Do the y = 1.0*A*x +0.0*y by mkl\n");
	//printf("nrow=%d, ncol=%d, ldx=%d, ldy=%d\n", mat->nrows, mat->ncols, x->ldd, y->ldd);
	/*   Computes y = alpha * A * x + beta * y   */
	mkl_sparse_d_mm (
			SPARSE_OPERATION_NON_TRANSPOSE,
			1.0,  //alpha 
			csrA, descr, SPARSE_LAYOUT_COLUMN_MAJOR,
			x->data+start[0]*x->ldd, //x
			num_vec, //columns (要做多少列X和Y)
			x->ldd,  //ldx
			0.0,     //beta
		    y->data+start[1]*y->ldd, //y
		    y->ldd); //ldy
#endif
	mkl_sparse_destroy (csrA);

#else
	memset(y->data+(y->ldd)*start[1],0,(y->ldd)*num_vec*sizeof(double));
#if OPS_USE_OMP
	#pragma omp parallel for schedule(static) num_threads(OMP_NUM_THREADS)
#endif
	for (col = 0; col < num_vec; ++col) {
		int i, j;
		double *dm, *dx, *dy; int *i_row;
		dm = mat->data; i_row = mat->i_row;
		dx = x->data+(x->ldd)*(start[0]+col);
		dy = y->data+(y->ldd)*(start[1]+col);
		for (j = 0; j < mat->ncols; ++j, ++dx) {
			for (i = mat->j_col[j]; i < mat->j_col[j+1]; ++i) {
				dy[*i_row++] += (*dm++)*(*dx);
			}
		}
	}
#endif
	}
	else {
		ops->lapack_ops->MultiVecAxpby (1.0, (void **)x, 0.0, (void **)y, 
				start, end, ops->lapack_ops);
	}
	return;
}


static void MatTransDotMultiVec (CCSMAT *mat, LAPACKVEC *x, 
		LAPACKVEC *y, int *start, int *end, struct OPS_ *ops)
{
	assert(end[0]-start[0]==end[1]-start[1]);
	assert(y->nrows==y->ldd);
	assert(x->nrows==x->ldd);
	assert(mat->nrows==mat->ncols);
	/* Only for 对称矩阵 */
	MatDotMultiVec (mat, x, y, start, end, ops);
	return;
}
static void VecCreateByMat (LAPACKVEC **des_vec, CCSMAT *src_mat, struct OPS_ *ops)
{
	MultiVecCreateByMat(des_vec,1,src_mat, ops);
	return;
}
static void MatDotVec (CCSMAT *mat, LAPACKVEC *x, LAPACKVEC *y, struct OPS_ *ops)
{
	int start[2] = {0,0}, end[2] = {1,1};
	MatDotMultiVec(mat,x,y,start,end, ops);
	return;
}
static void MatTransDotVec (CCSMAT *mat, LAPACKVEC *x, LAPACKVEC *y, struct OPS_ *ops)
{
	int start[2] = {0,0}, end[2] = {1,1};
	MatTransDotMultiVec(mat,x,y,start,end, ops);
	return;
}

/* Encapsulation */
static void CCS_MatView (void *mat, struct OPS_ *ops)
{
	MatView ((CCSMAT *)mat, ops);
	return;
}
/* vec */
static void CCS_VecCreateByMat (void **des_vec, void *src_mat, struct OPS_ *ops)
{
	VecCreateByMat ((LAPACKVEC **)des_vec, (CCSMAT *)src_mat, ops);
	return;
}
static void CCS_MatDotVec (void *mat, void *x, void *y, struct OPS_ *ops)
{
	MatDotVec ((CCSMAT *)mat, (LAPACKVEC *)x, (LAPACKVEC *)y, ops);
	return;
}
static void CCS_MatTransDotVec (void *mat, void *x, void *y, struct OPS_ *ops)
{
	MatTransDotVec ((CCSMAT *)mat, (LAPACKVEC *)x, (LAPACKVEC *)y, ops);
	return;
}
/* multi-vec */
static void CCS_MultiVecCreateByMat (void ***des_vec, int num_vec, void *src_mat, struct OPS_ *ops)
{
	MultiVecCreateByMat ((LAPACKVEC **)des_vec, num_vec, (CCSMAT *)src_mat, ops);		
	return;
}

static void CCS_MatDotMultiVec (void *mat, void **x, 
		void **y, int *start, int *end, struct OPS_ *ops)
{
	MatDotMultiVec ((CCSMAT *)mat, (LAPACKVEC *)x, 
			(LAPACKVEC *)y, start, end, ops);
	return;
}
static void CCS_MatTransDotMultiVec (void *mat, void **x, 
		void **y, int *start, int *end, struct OPS_ *ops)
{
	MatTransDotMultiVec ((CCSMAT *)mat, (LAPACKVEC *)x, 
			(LAPACKVEC *)y, start, end, ops);
	return;
}
/*
static void CCS_MatAxpby(double alpha, void* matA, double beta, void* matB, struct OPS_* ops)
{

	CCSMAT* A = (CCSMAT*)matA;
	CCSMAT* B = (CCSMAT*)matB;
	int *A_i_row = A->i_row, *A_j_col = A->j_col, *B_i_row = B->i_row, *B_j_col = B->j_col;
	int Anrows = A->nrows, Ancols = A->ncols, Bnrows = B->nrows, Bncols = B->ncols;
	int Arow, Brow;  
	double *Adata = A->data, *Bdata = B->data;
	assert(Anrows == Bnrows || Ancols == Bncols);

	int i, j, posA, posB, startA, startB, endA, endB;
	for (i = 0; i < Ancols; i++)
	{
		
		startA = A_j_col[i]; endA = A_j_col[i + 1];
		startB = B_j_col[i]; endB = B_j_col[i + 1];
		posA = startA; posB = startB;
		//printf("The column number: %d, startA: %d, endA: %d, startB: %d, endB: %d:\n", i, startA, endA, startB, endB);  
		while (posB < endB)
		{
			//check the row number
			Brow = B_i_row[posB];
			Arow = A_i_row[posA];
			//printf("Brow = %d\n", Brow);
			while(Arow <= Brow && posA < endA )
			{
				//printf("The Arow = %d,  ", Arow);
				if (Arow == Brow)
				{
					//printf("do the combination!\n");				
					Bdata[posB] = alpha * Adata[posA] + beta * Bdata[posB];
				}
				posA++; 
				if(posA < endA)
					Arow = A_i_row[posA];
			}
			posB ++; 
		}
	}
}
*/
/*
 static void CCS_MatAxpby(double alpha, void* matA, double beta, void* matB, struct OPS_* ops)
{
	LAPACKVEC* matA_vec, * matB_vec;
	ops->MultiVecCreateByMat((void***)(&matA_vec), ((CCSMAT*)matA)->ncols, (CCSMAT*)matA, ops);
	ops->MultiVecCreateByMat((void***)(&matB_vec), ((CCSMAT*)matB)->ncols, (CCSMAT*)matB, ops);
	int col, i; double* destinA, * destinB;
	for (col = 0; col < ((CCSMAT*)matA)->ncols; ++col) {
		for (i = ((CCSMAT*)matA)->j_col[col]; i < ((CCSMAT*)matA)->j_col[col + 1]; ++i) {
			destinA = matA_vec->data + (matA_vec->ldd) * col + ((CCSMAT*)matA)->i_row[i];
			*destinA = ((CCSMAT*)matA)->data[i];
		}
	}
	for (col = 0; col < ((CCSMAT*)matB)->ncols; ++col) {
		for (i = ((CCSMAT*)matB)->j_col[col]; i < ((CCSMAT*)matB)->j_col[col + 1]; ++i) {
			destinB = matB_vec->data + (matB_vec->ldd) * col + ((CCSMAT*)matB)->i_row[i];
			*destinB = ((CCSMAT*)matB)->data[i];
		}
	}
	int start[2] = { 0,0 }, end[2] = { ((CCSMAT*)matA)->ncols,((CCSMAT*)matA)->ncols };
	ops->MultiVecAxpby(beta, (void**)matB_vec, alpha, (void**)matA_vec, start, end, ops);
	int ind_Avec = 0, ind_matA;
	for (col = 0; col < ((CCSMAT*)matA)->ncols; ++col) {
		for (i = ((CCSMAT*)matA)->j_col[col]; i < ((CCSMAT*)matA)->j_col[col + 1]; ++i) {
			ind_matA = (matA_vec->ldd) * col + ((CCSMAT*)matA)->i_row[i];
			((CCSMAT*)matA)->data[ind_Avec++] = matA_vec->data[ind_matA];
		}
	}
	ops->lapack_ops->MultiVecDestroy((void***)(&matA_vec), ((CCSMAT*)matA)->ncols, ops->lapack_ops);
	ops->lapack_ops->MultiVecDestroy((void***)(&matB_vec), ((CCSMAT*)matB)->ncols, ops->lapack_ops);
	return;
}
*/


void OPS_CCS_Set (struct OPS_ *ops)
{
	assert(ops->lapack_ops==NULL);
	OPS_Create (&(ops->lapack_ops));
	OPS_LAPACK_Set (ops->lapack_ops);
	ops->Printf                   = DefaultPrintf;
	ops->GetOptionFromCommandLine = DefaultGetOptionFromCommandLine;
	ops->GetWtime                 = DefaultGetWtime;
	ops->MatView                  = CCS_MatView;
	/* vec */
	ops->VecCreateByMat           = CCS_VecCreateByMat;
	ops->VecCreateByVec           = ops->lapack_ops->VecCreateByVec   ;
	ops->VecDestroy               = ops->lapack_ops->VecDestroy       ;
	ops->VecView                  = ops->lapack_ops->VecView          ;
	ops->VecInnerProd             = ops->lapack_ops->VecInnerProd     ;
	ops->VecLocalInnerProd        = ops->lapack_ops->VecLocalInnerProd;
	ops->VecSetRandomValue        = ops->lapack_ops->VecSetRandomValue;
	ops->VecAxpby                 = ops->lapack_ops->VecAxpby         ;
	ops->MatDotVec                = CCS_MatDotVec     ;
	ops->MatTransDotVec           = CCS_MatTransDotVec;
	/* multi-vec */
	ops->MultiVecCreateByMat      = CCS_MultiVecCreateByMat;
	ops->MultiVecCreateByVec      = ops->lapack_ops->MultiVecCreateByVec     ;
	ops->MultiVecCreateByMultiVec = ops->lapack_ops->MultiVecCreateByMultiVec;
	ops->MultiVecDestroy          = ops->lapack_ops->MultiVecDestroy         ;
	ops->GetVecFromMultiVec       = ops->lapack_ops->GetVecFromMultiVec      ;
	ops->RestoreVecForMultiVec    = ops->lapack_ops->RestoreVecForMultiVec   ;
	ops->MultiVecView             = ops->lapack_ops->MultiVecView            ;
	ops->MultiVecLocalInnerProd   = ops->lapack_ops->MultiVecLocalInnerProd  ;
	ops->MultiVecInnerProd        = ops->lapack_ops->MultiVecInnerProd       ;
	ops->MultiVecSetRandomValue   = ops->lapack_ops->MultiVecSetRandomValue  ;
	ops->MultiVecAxpby            = ops->lapack_ops->MultiVecAxpby           ;
	ops->MultiVecLinearComb       = ops->lapack_ops->MultiVecLinearComb      ;
	ops->MatDotMultiVec           = CCS_MatDotMultiVec     ;
	ops->MatTransDotMultiVec      = CCS_MatTransDotMultiVec;
	//ops->MatAxpby				  = CCS_MatAxpby;
	return;
}

